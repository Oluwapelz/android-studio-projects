package com.example.toyin.tinker.ModelClasses;

/**
 * Created by Toyin on 18/04/2017.
 */
public class UserDetails {

    public String Playername;
    public int score;

    public UserDetails(String playername, int score) {
        this.Playername = playername;
        this.score = score;
    }
    public void setName(String playername){
        Playername = playername;
    }

    public String getName(){
        return Playername;
    }

    public void setScore(int score1){
        score = score1;
    }

    public int getScore(){
        return score;
    }

}