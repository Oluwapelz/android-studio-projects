package com.example.toyin.tinker.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.toyin.tinker.ModelClasses.UserDetails;
import com.example.toyin.tinker.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;

/**
 * Created by Toyin on 06/01/2017.
 */
public class ClassFinish extends AppCompatActivity {
    public LinearLayout start_again, menu_button;
    public TextView name, gotten, attempted;
    public int got, attempt;
    public String Player;
    public FirebaseDatabase players_database;
    public DatabaseReference players_database_reference;
    public UserDetails users;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish_page);
        //Set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //SEt the textView in the xml file
        name =(TextView) findViewById(R.id.name);
        gotten = (TextView) findViewById(R.id.answers_gotten);
        attempted = (TextView) findViewById(R.id.attempted_question);

        got = getIntent().getIntExtra("gotten", 0);
        attempt = getIntent().getIntExtra("attempted", 0);
        //Show a toast message after the player exits the question session.
        if(got == attempt){
            Toast.makeText(getApplicationContext(), "Genius", Toast.LENGTH_SHORT).show();
        }
        else if(got == (attempt / 2)){
            Toast.makeText(getApplicationContext(), "Cool, You tried. Try harder next time", Toast.LENGTH_LONG).show();
        }
        else if(got == ((attempt / 3) * 2)){
            Toast.makeText(getApplicationContext(), "Just a little more harder", Toast.LENGTH_LONG).show();
        }
        else if(got == ((attempt / 3) * 1)){
            Toast.makeText(getApplicationContext(), "Hmm, just a little more trial", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Try hard next time!!!", Toast.LENGTH_LONG).show();
        }



        gotten.setText("  " + got + "  ");
        attempted.setText("" + attempt);

        Player = getIntent().getStringExtra("player_name");


//        HashMap<Integer, UserDetails> user = new HashMap<>();
//        HashMap<String, Integer> user_data = new HashMap<>();
//        user_data.put(Player, got);
//
        //Using the UserDetails class
//        UserDetails user = new UserDetails(Player, got);
//        int count = 0;
//        user.put(1, users.setName(Player));

        //Set the name to the name here.
        name.setText(Player);

        start_again = (LinearLayout) findViewById(R.id.start_again);
        menu_button = (LinearLayout) findViewById(R.id.menu_button);

        //Set a listener to the play again button.
        start_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassFinish.this, DifficultActivity.class);
                intent.putExtra("player_name", Player);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        menu_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassFinish.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

//        displayDatabaseInfo();
        //Set an instance of the firebase database.
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //Get and instance of the database.
//        players_database.getInstance();

        //Reference the database
        players_database_reference = players_database.getInstance().getReference("players");

        //Adding an event listener
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

//        Query myPlayerQuery = players_database_reference.child();

        //Add this event listener to the database reference
        players_database_reference.addChildEventListener(childEventListener);

    }


    @Override
    public void onBackPressed(){
        //Do nothing if player clicks the back button.
        Toast.makeText(getApplicationContext(), "Pal, you ain't going back, that's a cheat", Toast.LENGTH_LONG).show();
    }
}
